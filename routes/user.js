const express = require("express");
const utils = require("../utils");
const router = express.Router();

router.post("/add", (request, response) => {
  const { name, email, salary, age } = request.body;

  const connection = utils.openConnection();

  const statement = `
        insert into user
          ( name,email,salary,age)
        values
          ( '${name}','${email}',${salary},${age})
      `;
  connection.query(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.get("/:name", (request, response) => {
  const { name } = request.params;

  const connection = utils.openConnection();

  const statement = `
        select * from user where 
        name = '${name}'
      `;
  connection.query(statement, (error, result) => {
    if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("user not found");
    }
  });
});

router.delete("/delete/:name", (request, response) => {
  const { name } = request.params;

  const connection = utils.openConnection();

  const statement = `
        delete from user where 
        name = '${name}' 
      `;
  connection.query(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:name", (request, response) => {
  const { name } = request.params;

  const connection = utils.openConnection();

  const statement = `
        delete from user where 
        name = '${name}' 
      `;
  connection.query(statement, (error, result) => {
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
