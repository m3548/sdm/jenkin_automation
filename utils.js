function createResult(error, data) {
  const result = {};
  if (error) {
    result["status"] = "error";

    result["error"] = error;
  } else {
    result["status"] = "success";
    result["data"] = data;
  }

  return result;
}

const mysql = require("mysql");

const openConnection = () => {
  const connection = mysql.createConnection({
    port: 3306,
    host: "localhost",
    // uri: "mysql://db:3306",
    user: "root",
    password: "root",
    database: "test_db",
  });

  connection.connect();

  return connection;
};

module.exports = {
  createResult,
  openConnection,
};
